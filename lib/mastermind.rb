class Code

  PEGS = {
    "R" => :red,
    "G" => :green,
    "B" => :blue,
    "Y" => :yellow,
    "O" => :orange,
    "P" => :purple
  }

  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end

  def self.parse(string)
    parsed_pegs = string.chars.map do |char|
      raise "Invalid Colors." unless PEGS.include?(char.upcase)
      PEGS[char.upcase]
    end
    Code.new(parsed_pegs)
  end

  def self.random
    random_pegs = []
    4.times { random_pegs << PEGS.values.sample }
    Code.new(random_pegs)
  end

  def [](idx)
    pegs[idx]
  end

  def exact_matches(code)
    if self.pegs.each.any? { |peg| code.pegs.include?(peg) }
      matches_count = 0
      self.pegs.each_with_index do |peg, idx|
        if peg == code.pegs[idx]
          matches_count += 1
        end
      end
      return matches_count if matches_count > 0
    end
    0
  end

  def near_matches(code)
    near_matches_count = 0
    uniq_pegs = []
    self.pegs.each_with_index do |peg1, idx1|
      if code.pegs.include?(peg1)
        code.pegs.each_with_index do |peg2, idx2|
          if peg2 == peg1 && idx1 != idx2 && code.pegs[idx2] == peg2
            near_matches_count += 1 unless uniq_pegs.include?(peg2)
            uniq_pegs << peg2 unless uniq_pegs.include?(peg2)
          end
        end
      end
    end
    near_matches_count
  end

  def ==(obj)
    if obj.class != Code
      false
    elsif self.pegs != obj.pegs
      false
    else
      true
    end
  end

end

class Game

  attr_reader :secret_code
  attr_accessor :guess

  def initialize(secret_code=Code.random)
    @secret_code = secret_code
  end

  def play
    turns_left = 10
    while turns_left > 0
      get_guess
      print secret_code.pegs.to_s
      if secret_code == @guess
        puts "You guessed correctly! The secret_code was #{secret_code.pegs.to_s}"
        exit
      end
      self.display_matches(@guess)
      turns_left -= 1
    end
    puts "You are out of turns. The secret code was #{secret_code.pegs.to_s}"
    exit
  end

  def get_guess
    puts "What is your guess in RGBY format?
    (ex. RGBY (red, green, blue, yellow), RRRG (red, red, red, green),
    BOPY (blue, orange, purple, yellow), etc.): "
    guess = gets.chomp
    @guess = Code.parse(guess)
  end

  def display_matches(code)
    near_matches_count = secret_code.near_matches(code)
    exact_matches_count = secret_code.exact_matches(code)
    puts "Number of near matches: #{near_matches_count}"
    puts "Number of exact matches: #{exact_matches_count}"
  end

end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
